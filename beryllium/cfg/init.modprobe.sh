#!/bin/bash
# Copyright (c) 2023 Diemit <598757652@qq.com>
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# modprobe gpi
# modprobe i2c-qcom-geni
# modprobe qcom_pmi8998_charger
# modprobe qcom_fg
# #EBBG
# modprobe focaltech_fts edt-ft5x06re
# #Tianma
# modprobe nt36xxx
# modprobe dm-mod
# modprobe qcom_sysmon
# modprobe icc-bwmon
# modprobe qcom_pil_info
# # modprobe qcom-wdt
# modprobe qcom_glink_smem
# modprobe qcom_q6v5
# modprobe qcom_common
# modprobe qcom_q6v5_pas
# modprobe slim-qcom-ngd-ctrl
# modprobe icc-osm-l3
# modprobe qrtr
# modprobe rfkill
# modprobe qcom_q6v5_mss
# modprobe cfg80211
# modprobe qcom-rng
# modprobe ath
# modprobe joydev
# modprobe libdes
# modprobe ecc
# modprobe authenc
# modprobe camcc-sdm845
# modprobe libarc4
# modprobe snd-soc-tas2559
# modprobe coresight
# modprobe ipa
# modprobe ecdh_generic
# modprobe stm_core
# modprobe coresight-funnel
# modprobe qcrypto
# modprobe coresight-replicator
# modprobe mac80211
# modprobe coresight-tmc
# modprobe coresight-stm
# modprobe mc
# modprobe videodev
# modprobe bluetooth

# modprobe videobuf2-common
# modprobe qcom_stats
# modprobe ath10k_core
# modprobe reset-qcom-pdc
# modprobe btqca
# modprobe videobuf2-v4l2
# modprobe btbcm
# modprobe ath10k_snoc
# modprobe v4l2-mem2mem
# modprobe hci_uart
# modprobe crct10dif-ce
# modprobe snd-soc-rl6231
# modprobe rtc-pm8xxx
# modprobe qcom-spmi-rradc
# modprobe qcom-spmi-haptics
# modprobe snd-soc-rt5663
# modprobe venus-core
# modprobe soundwire-bus
# modprobe snd-soc-qcom-common
# modprobe snd-soc-sdm845
# modprobe led-class-multicolor
# modprobe leds-qcom-lpg
# modprobe rpmsg_ctrl
# modprobe qrtr-smd
# modprobe fastrpc
# modprobe videobuf2-memops
# modprobe videobuf2-dma-contig
# modprobe venus-dec
# modprobe venus-enc
# modprobe regmap-slimbus
# modprobe wcd934x
# modprobe snd-soc-wcd-mbhc
# modprobe soundwire-qcom
# modprobe gpio-wcd934x
# modprobe snd-soc-wcd934x
# modprobe ipv6
# modprobe q6core
# modprobe q6voice-common
# modprobe q6cvs
# modprobe q6mvm
# modprobe snd-q6dsp-common
# modprobe q6cvp
# modprobe q6afe
# modprobe q6asm
# modprobe q6adm
# modprobe q6afe-dai
# modprobe q6routing
# modprobe q6asm-dai
# modprobe q6voice
# modprobe q6voice-dai
# modprobe af_alg
# modprobe algif_skcipher
# modprobe algif_hash
# modprobe nfnetlink
# modprobe nf_tables
# modprobe nf_defrag_ipv6
# modprobe nf_defrag_ipv4
# modprobe nf_conntrack
# modprobe nft_ct
# modprobe nft_reject
# modprobe nf_reject_ipv6
# modprobe nf_reject_ipv4
# modprobe nft_reject_inet
# modprobe zsmalloc
# modprobe zram
# modprobe zstd