
**补丁说明**

**1、通用补丁**

device/board/opc/common/patches

添加soc仓子系统补丁：
001-build.diff -> build

selinux关闭导致函数未调用修复补丁：
002-base-startup-appspawn.diff -> base/startup/appspawn

appspawn修复补丁：
004-foundation-ability-ability_runtime.diff -> foundation/ability/ability_runtime

小型系统编译修复补丁：
003-foundation-arkui-gce_engine_lite.diff -> foundation/arkui/ace_engine_lite