#!/bin/bash
# Copyright (c) 2023 Diemit <598757652@qq.com>
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

modprobe mdt_loader
modprobe drm_dp_aux_bus
modprobe gpu-sched
modprobe cfbfillrect
modprobe syscopyarea
modprobe cfbimgblt
modprobe sysfillrect
modprobe sysimgblt
modprobe fb_sys_fops
modprobe cfbcopyarea
modprobe drm_kms_helper
modprobe drm_display_helper
modprobe msm
modprobe i2c-qup
modprobe rmi_core
modprobe rmi_i2c
modprobe qcom-spmi-haptics
modprobe dm-mod
modprobe rmtfs_mem
modprobe evdev
modprobe qcom_hwspinlock
modprobe smp2p
modprobe qcom_pil_info
modprobe qmi_helpers
modprobe qcom_sysmon
modprobe qcom_smd
modprobe qcom_common
modprobe qcom_q6v5
modprobe qcom_stats
modprobe rfkill
modprobe qcom_q6v5_pas
modprobe rtc-pm8xxx
modprobe qcom_q6v5_mss
modprobe qcom_fg
modprobe led-class
modprobe led-class-multicolor
modprobe qrtr
modprobe cfg80211
modprobe leds-qcom-lpg
modprobe ath
modprobe libarc4
modprobe mac80211
modprobe serial_core
modprobe sha256-arm64
modprobe sha2-ce
modprobe sha1_generic
modprobe msm_serial
modprobe sha1-ce
modprobe gf128mul
modprobe polyval-generic
modprobe polyval-ce
modprobe ath10k_core
modprobe libaes
modprobe aes-ce-cipher
modprobe ghash-ce
modprobe ath10k_snoc
modprobe aes-ce-blk
modprobe qrtr-smd
modprobe ecc
modprobe ecdh_generic
modprobe bluetooth
modprobe btqca
modprobe hci_uart
modprobe ipv6
modprobe bnep
modprobe af_alg
modprobe algif_skcipher
modprobe algif_hash
modprobe nfnetlink
modprobe libcrc32c
modprobe nf_tables
modprobe nf_defrag_ipv6
modprobe nf_defrag_ipv4
modprobe nf_conntrack
modprobe nft_ct
modprobe nft_reject
modprobe nf_reject_ipv6
modprobe nf_reject_ipv4
modprobe nft_reject_inet
modprobe zsmalloc
modprobe zram
